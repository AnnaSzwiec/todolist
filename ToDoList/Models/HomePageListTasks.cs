﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoList.Models
{
    class HomePageListTasks
    {
        public List<Task> ListTasks { get; set; }

        public HomePageListTasks ()
        {
            ListTasks = new List<Task>();
            ListTasks.Add(new Task("Task1", DateTime.Now, TimeSpan.Zero, "aqq1"));
            ListTasks.Add(new Task("Task2", DateTime.Now.AddDays(1), TimeSpan.Zero, "aqq2"));
            ListTasks.Add(new Task("Task3", DateTime.Now.AddDays(2), TimeSpan.Zero, "aqq3"));
            ListTasks.Add(new Task("Task4", DateTime.Now.AddDays(3), TimeSpan.Zero, "aqq4"));
            ListTasks.Add(new Task("Task5", DateTime.Now.AddDays(3), TimeSpan.Zero, "aqq5"));
        }
    }
}
