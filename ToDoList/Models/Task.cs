﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToDoList.Models
{
    class Task
    {
        private int id;

        public string Name { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan Duration { get; set; }
        public string Description { get; set; }

        public Task()
        {
            Name = string.Empty;
            Date = DateTime.Now;
            Duration = TimeSpan.Zero;
            Description = string.Empty;
        }

        public Task( string name, DateTime date, TimeSpan duration, string description)
        {
            Name = name;
            Date = date;
            Duration = duration;
            Description = description;
        }

    }
}
